using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GeometricTransformationLab : MonoBehaviour
{
    public Image originalImageDisplay;
    public Image processedImageDisplay;

    public Dropdown transformationDropdown;

    public Text modifiedImageText;

    private Texture2D _originalImage;
    private Texture2D _processedImage;

    public loadingbar loadingbar;
    public GameObject loadingObject;

    private void Start()
    {
        // Load the original image at the start
        _originalImage = originalImageDisplay.sprite.texture;
    }

    /*public void LoadOriginalImage()
    {
        string imagePath = "";

        // Check the current platform
        if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor)
        {
            // Load image from file on PC
            imagePath = UnityEditor.EditorUtility.OpenFilePanel("Select Image", "", "png,jpg");
        }
        else if (Application.platform == RuntimePlatform.Android ||
                 Application.platform == RuntimePlatform.IPhonePlayer)
        {
            // Show a button to trigger the file picker dialog on mobile
            GameObject filePickerButton = new GameObject("FilePickerButton");
            filePickerButton.transform.SetParent(this.transform, false);

            Button button = filePickerButton.AddComponent<Button>();
            button.onClick.AddListener(() => OpenFilePicker());

            Text buttonText = filePickerButton.AddComponent<Text>();
            buttonText.text = "Select Image";
            buttonText.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
            buttonText.fontSize = 20;

            // Hide the original image display until an image is selected
            originalImageDisplay.gameObject.SetActive(false);
        }
        else
        {
            Debug.LogError("Unsupported platform! Cannot load image.");
            return;
        }

        // Load the image from the specified path (for PC platforms)
        if (!string.IsNullOrEmpty(imagePath))
        {
            StartCoroutine(LoadImageCoroutine(imagePath));
        }
    }*/

    /*private void OpenFilePicker()
    {
        // Trigger the file picker dialog
        StartCoroutine(PickImageCoroutine());
    }*/

    /*private IEnumerator PickImageCoroutine()
    {
        // Request permission to access the device's gallery
        yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);

        if (Application.HasUserAuthorization(UserAuthorization.WebCam))
        {
            // Show the file picker dialog
            var path = UnityEditor.EditorUtility.OpenFilePanel("Select Image", "", "png,jpg,jpeg");

            if (!string.IsNullOrEmpty(path))
            {
                // Load the image from the selected path
                StartCoroutine(LoadImageCoroutine(path));
            }
        }
        else
        {
            Debug.LogError("Permission denied! Cannot load image.");
        }
    }

    private IEnumerator LoadImageCoroutine(string imagePath)
    {
        // Create a WWW object to load the image
        WWW www = new WWW("file://" + imagePath);

        // Wait until the image is loaded
        yield return www;

        if (string.IsNullOrEmpty(www.error))
        {
            // Assign the loaded image to the originalImage variable
            _originalImage = www.texture;

            // Set the original image display sprite as the loaded image
            originalImageDisplay.sprite = Sprite.Create(_originalImage,
                new Rect(0, 0, _originalImage.width, _originalImage.height), Vector2.zero);

            // Show the original image display
            originalImageDisplay.gameObject.SetActive(true);
        }
        else
        {
            Debug.LogError("Failed to load image: " + www.error);
        }
    }*/

    public void ApplyTransformation()
    {
        if (_originalImage == null)
        {
            Debug.LogError("No original image loaded!");
            return;
        }

        // Enable the loading object
        loadingObject.SetActive(true);
        loadingbar.SetLoadValue(0f);

        // Disable the apply button while transforming the image
        transformationDropdown.interactable = false;

        // Get the selected transformation from the dropdown
        string selectedTransformation = transformationDropdown.options[transformationDropdown.value].text;

        // Apply the selected transformation based on the chosen parameters
        StartCoroutine(TransformImageCoroutine(selectedTransformation));
    }

    private IEnumerator TransformImageCoroutine(string selectedTransformation)
    {
        // Wait for a frame to ensure the loading object is displayed
        yield return null;

        // Apply the transformation asynchronously
        Texture2D processedImage = null;
        switch (selectedTransformation)
        {
            case "Mirror":
                processedImage = MirrorTransform(_originalImage);
                break;
            case "Rotation":
                processedImage = RotationTransform(_originalImage, 45);
                break;
            case "Scale":
                processedImage = ScaleTransform(_originalImage, 1.6f, 0.9f);
                break;
            case "Shear":
                processedImage = ShearTransform(_originalImage, 0.5f);
                break;
            case "Polynomial":
                processedImage = PolynomialTransform(_originalImage);
                break;
            default:
                Debug.LogError("Invalid transformation selected!");
                yield break;
        }

        // Wait until the transformation is completed
        yield return null;

        // Update the processed image display with the transformed image
        processedImageDisplay.sprite = Sprite.Create(processedImage,
            new Rect(0, 0, processedImage.width, processedImage.height), Vector2.zero);

        // Update the modified image text
        switch (selectedTransformation)
        {
            case "Mirror":
                modifiedImageText.text = "Mirrored Image";
                break;
            case "Rotation":
                modifiedImageText.text = "Rotated Image";
                break;
            case "Scale":
                modifiedImageText.text = "16:9 Scaled Image";
                break;
            case "Shear":
                modifiedImageText.text = "Sheared Image";
                break;
            case "Polynomial":
                modifiedImageText.text = "Polynomial";
                break;
        }
        
        // Disable the loading object
        loadingObject.SetActive(false);

        // Enable the apply button after transformation is completed
        transformationDropdown.interactable = true;
    }


    private Texture2D MirrorTransform(Texture2D sourceImage)
    {
        Texture2D mirroredImage = new Texture2D(sourceImage.width, sourceImage.height);

        for (int x = 0; x < sourceImage.width; x++)
        {
            for (int y = 0; y < sourceImage.height; y++)
            {
                Color pixelColor = sourceImage.GetPixel(x, y);

                // Mirror the image horizontally
                int mirroredX = sourceImage.width - x - 1;

                // Set the pixel color of the mirrored image
                mirroredImage.SetPixel(mirroredX, y, pixelColor);
                
                float loadValue = (float)(x + 1) / sourceImage.height;
                loadingbar.SetLoadValue(loadValue);
            }
        }

        mirroredImage.Apply();
        return mirroredImage;
    }

    private Texture2D RotationTransform(Texture2D sourceImage, float angle)
    {
        // Convert the angle from degrees to radians
        float radianAngle = angle * Mathf.Deg2Rad;

        // Calculate the rotation center
        Vector2 center = new Vector2(sourceImage.width / 2f, sourceImage.height / 2f);

        // Calculate the dimensions of the rotated image
        float diagonal = Mathf.Sqrt(sourceImage.width * sourceImage.width + sourceImage.height * sourceImage.height);
        int rotatedWidth = Mathf.RoundToInt(diagonal);
        int rotatedHeight = Mathf.RoundToInt(diagonal);

        // Create a new Texture2D for storing the rotated image
        Texture2D rotatedImage = new Texture2D(rotatedWidth, rotatedHeight);

        // Calculate the scaling factor for zooming the rotated image to fit the original image size
        float scale = 1f;

        // Calculate the position of the center in the rotated image
        Vector2 rotatedCenter = new Vector2(rotatedWidth / 2f, rotatedHeight / 2f);

        // Iterate over each pixel in the rotated image and calculate the corresponding pixel in the source image
        for (int x = 0; x < rotatedWidth; x++)
        {
            for (int y = 0; y < rotatedHeight; y++)
            {
                // Calculate the offset from the rotated center
                Vector2 offset = new Vector2(x - rotatedCenter.x, y - rotatedCenter.y);

                // Apply the rotation matrix to the offset
                Vector2 rotatedOffset = RotateVector2(offset, radianAngle);

                // Scale the rotated offset to fit the original image size
                rotatedOffset *= scale;

                // Calculate the position of the corresponding pixel in the source image
                int sourceX = Mathf.RoundToInt(rotatedOffset.x + center.x);
                int sourceY = Mathf.RoundToInt(rotatedOffset.y + center.y);

                // Check if the calculated position is within the bounds of the source image
                if (sourceX >= 0 && sourceX < sourceImage.width && sourceY >= 0 && sourceY < sourceImage.height)
                {
                    // Get the color of the corresponding pixel in the source image and assign it to the rotated image
                    Color pixelColor = sourceImage.GetPixel(sourceX, sourceY);
                    rotatedImage.SetPixel(x, y, pixelColor);
                }
                else
                {
                    // If the calculated position is outside the bounds of the source image, assign a transparent color
                    rotatedImage.SetPixel(x, y, Color.black);
                }
                
                float loadValue = (float)(x + 1) / sourceImage.height;
                loadingbar.SetLoadValue(loadValue);
            }
        }

        // Apply the changes to the rotated image
        rotatedImage.Apply();

        return rotatedImage;
    }

    private Vector2 RotateVector2(Vector2 vector, float angle)
    {
        float cosAngle = Mathf.Cos(angle);
        float sinAngle = Mathf.Sin(angle);

        float x = vector.x * cosAngle - vector.y * sinAngle;
        float y = vector.x * sinAngle + vector.y * cosAngle;

        return new Vector2(x, y);
    }

    private Texture2D ScaleTransform(Texture2D sourceImage, float scaleX, float scaleY)
    {
        int targetWidth = Mathf.RoundToInt(sourceImage.width * scaleX);
        int targetHeight = Mathf.RoundToInt(sourceImage.height * scaleY);

        Texture2D scaledImage = new Texture2D(targetWidth, targetHeight);

        for (int x = 0; x < targetWidth; x++)
        {
            for (int y = 0; y < targetHeight; y++)
            {
                // Calculate the corresponding pixel position in the source image
                float sourceX = x / scaleX;
                float sourceY = y / scaleY;

                // Sample the pixel color from the source image using bilinear interpolation
                Color pixelColor = BilinearInterpolation(sourceImage, sourceX, sourceY);

                // Set the pixel color of the scaled image
                scaledImage.SetPixel(x, y, pixelColor);
                
                float loadValue = (float)(y + 1) / sourceImage.height;
                loadingbar.SetLoadValue(loadValue);
            }
        }

        scaledImage.Apply();
        return scaledImage;
    }

    private Color BilinearInterpolation(Texture2D image, float x, float y)
    {
        int x0 = Mathf.FloorToInt(x);
        int y0 = Mathf.FloorToInt(y);
        int x1 = x0 + 1;
        int y1 = y0 + 1;

        // Check if the calculated positions are within the bounds of the source image
        bool validX0 = x0 >= 0 && x0 < image.width;
        bool validY0 = y0 >= 0 && y0 < image.height;
        bool validX1 = x1 >= 0 && x1 < image.width;
        bool validY1 = y1 >= 0 && y1 < image.height;

        // Retrieve the pixel colors of the surrounding pixels
        Color c00 = validX0 && validY0 ? image.GetPixel(x0, y0) : Color.black;
        Color c01 = validX0 && validY1 ? image.GetPixel(x0, y1) : Color.black;
        Color c10 = validX1 && validY0 ? image.GetPixel(x1, y0) : Color.black;
        Color c11 = validX1 && validY1 ? image.GetPixel(x1, y1) : Color.black;

        float tx = x - x0;
        float ty = y - y0;

        // Perform bilinear interpolation
        Color interpolatedColor = Color.Lerp(Color.Lerp(c00, c10, tx), Color.Lerp(c01, c11, tx), ty);

        return interpolatedColor;
    }

    private Texture2D ShearTransform(Texture2D sourceImage, float shearAmount)
    {
        // Calculate the width of the sheared image based on the original image's aspect ratio
        int shearedWidth = Mathf.RoundToInt(sourceImage.width + shearAmount * sourceImage.height);

        // Create a new texture with the sheared width and the same height as the original image
        Texture2D shearedImage = new Texture2D(shearedWidth, sourceImage.height);

        for (int x = 0; x < shearedWidth; x++)
        {
            for (int y = 0; y < sourceImage.height; y++)
            {
                // Calculate the sheared position based on the shear amount
                int shearedX = x - Mathf.RoundToInt(shearAmount * y);
                int shearedY = y;

                // Check if the sheared position is within the bounds of the source image
                if (shearedX >= 0 && shearedX < sourceImage.width && shearedY >= 0 && shearedY < sourceImage.height)
                {
                    // Get the color of the pixel at the sheared position and assign it to the sheared image
                    Color pixelColor = sourceImage.GetPixel(shearedX, shearedY);
                    shearedImage.SetPixel(x, y, pixelColor);
                }
                else
                {
                    // If the sheared position is outside the bounds of the source image, assign a transparent color
                    shearedImage.SetPixel(x, y, Color.black);
                }
                
                float loadValue = (float)(y + 1) / sourceImage.height;
                loadingbar.SetLoadValue(loadValue);
            }
        }

        shearedImage.Apply();

        // Create a temporary RenderTexture with the same dimensions as the original image
        RenderTexture renderTexture = RenderTexture.GetTemporary(sourceImage.width, sourceImage.height, 0,
            RenderTextureFormat.Default, RenderTextureReadWrite.Linear);
        renderTexture.filterMode = FilterMode.Point;
        RenderTexture.active = renderTexture;

        // Copy the sheared image to the RenderTexture using Graphics.Blit
        Graphics.Blit(shearedImage, renderTexture);

        // Create a new texture with the same dimensions as the original image
        Texture2D scaledImage = new Texture2D(sourceImage.width, sourceImage.height);

        // Read the pixels from the RenderTexture into the scaledImage texture
        scaledImage.ReadPixels(new Rect(0, 0, sourceImage.width, sourceImage.height), 0, 0);
        scaledImage.Apply();

        RenderTexture.active = null;
        RenderTexture.ReleaseTemporary(renderTexture);

        return scaledImage;
    }

    public Texture2D PolynomialTransform(Texture2D sourceImage)
    {
        Texture2D transformedImage = new Texture2D(sourceImage.width, sourceImage.height);

        for (int x = 0; x < sourceImage.width; x++)
        {
            for (int y = 0; y < sourceImage.height; y++)
            {
                // Calculate the transformed position based on a polynomial function (Disc to Square mapping)
                float transformedX =
                    x * Mathf.Sqrt(1f - (2f * y / sourceImage.height - 1f) * (2f * y / sourceImage.height - 1f));
                float transformedY =
                    y * Mathf.Sqrt(1f - (2f * x / sourceImage.width - 1f) * (2f * x / sourceImage.width - 1f));

                // Check if the transformed position is within the bounds of the source image
                if (transformedX >= 0 && transformedX < sourceImage.width && transformedY >= 0 &&
                    transformedY < sourceImage.height)
                {
                    // Get the color of the pixel at the transformed position and assign it to the transformed image
                    Color pixelColor =
                        sourceImage.GetPixel(Mathf.RoundToInt(transformedX), Mathf.RoundToInt(transformedY));
                    transformedImage.SetPixel(x, y, pixelColor);
                }
                else
                {
                    // If the transformed position is outside the bounds of the source image, assign a transparent color
                    transformedImage.SetPixel(x, y, Color.clear);
                }
                
                float loadValue = (float)(y + 1) / sourceImage.height;
                loadingbar.SetLoadValue(loadValue);
            }
        }

        transformedImage.Apply();
        return transformedImage;
    }
}
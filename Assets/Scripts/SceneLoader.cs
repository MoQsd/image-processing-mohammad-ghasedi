using UnityEngine;

public class SceneLoader : MonoBehaviour
{
    public string targetSceneName; // The name of the target scene
    public string objectToEnableName; // The name of the object to enable in the target scene

    public void LoadTargetScene()
    {
        // Store the object to enable name in PlayerPrefs or a static variable
        PlayerPrefs.SetString("ObjectToEnableName", objectToEnableName);

        // Load the target scene
        UnityEngine.SceneManagement.SceneManager.LoadScene(targetSceneName, UnityEngine.SceneManagement.LoadSceneMode.Single);
    }
}
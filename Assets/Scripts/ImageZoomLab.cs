using UnityEngine;
using UnityEngine.UI;

public class ImageZoomLab : MonoBehaviour
{
    public Texture2D originalImage;
    public RawImage displayImage;
    public Dropdown intensityDropdown;
    public Slider parameterSlider;

    private Texture2D processedImage;

    private void Start()
    {
        // Set the original image as the display image
        displayImage.texture = originalImage;
        displayImage.SetNativeSize();

        // Add listeners to the dropdown and slider
        intensityDropdown.onValueChanged.AddListener(OnIntensityDropdownValueChanged);
        parameterSlider.onValueChanged.AddListener(OnParameterSliderValueChanged);

        // Process the image with the initial intensity transformation
        ProcessImage();
    }

    private void OnIntensityDropdownValueChanged(int value)
    {
        // Process the image with the selected intensity transformation
        ProcessImage();
    }

    private void OnParameterSliderValueChanged(float value)
    {
        // Process the image with the updated parameter value
        ProcessImage();
    }

    private void ProcessImage()
    {
        // Get the selected intensity transformation
        IntensityTransformation transformation = GetSelectedTransformation();

        // Apply the intensity transformation to the original image
        processedImage = ApplyIntensityTransformation(originalImage, transformation);

        // Display the processed image
        displayImage.texture = processedImage;
        displayImage.SetNativeSize();
    }

    private IntensityTransformation GetSelectedTransformation()
    {
        IntensityTransformation transformation = IntensityTransformation.Linear;

        // Get the selected intensity transformation from the dropdown value
        switch (intensityDropdown.value)
        {
            case 0:
                transformation = IntensityTransformation.Linear;
                break;
            case 1:
                transformation = IntensityTransformation.Logarithmic;
                break;
            case 2:
                transformation = IntensityTransformation.Gamma;
                break;
            case 3:
                transformation = IntensityTransformation.PiecewiseLinear;
                break;
        }

        return transformation;
    }

    private Texture2D ApplyIntensityTransformation(Texture2D image, IntensityTransformation transformation)
    {
        Texture2D processedImage = new Texture2D(image.width, image.height);

        Color[] pixels = image.GetPixels();
        Color[] processedPixels = new Color[pixels.Length];

        // Apply the selected intensity transformation to each pixel
        for (int i = 0; i < pixels.Length; i++)
        {
            Color pixel = pixels[i];

            switch (transformation)
            {
                case IntensityTransformation.Linear:
                    processedPixels[i] = LinearTransformation(pixel);
                    break;
                case IntensityTransformation.Logarithmic:
                    processedPixels[i] = LogarithmicTransformation(pixel);
                    break;
                case IntensityTransformation.Gamma:
                    processedPixels[i] = GammaTransformation(pixel);
                    break;
                case IntensityTransformation.PiecewiseLinear:
                    processedPixels[i] = PiecewiseLinearTransformation(pixel);
                    break;
            }
        }

        processedImage.SetPixels(processedPixels);
        processedImage.Apply();

        return processedImage;
    }

    private Color LinearTransformation(Color pixel)
    {
        // Apply linear transformation to the pixel intensity
        // Adjust the intensity based on the parameter value
        float parameter = parameterSlider.value;
        float intensity = (pixel.r + pixel.g + pixel.b) / 3f;
        return new Color(intensity * parameter, intensity * parameter, intensity * parameter);
    }

    private Color LogarithmicTransformation(Color pixel)
    {
        // Apply logarithmic transformation to the pixel intensity
        // Adjust the intensity based on the parameter value
        float parameter = parameterSlider.value;
        float intensity = (pixel.r + pixel.g + pixel.b) / 3f;
        float adjustedIntensity = Mathf.Log(1f + parameter * intensity, 2f);
        return new Color(adjustedIntensity, adjustedIntensity, adjustedIntensity);
    }

    private Color GammaTransformation(Color pixel)
    {
        // Apply gamma transformation to the pixel intensity
        // Adjust the intensity based on the parameter value
        float parameter = parameterSlider.value;
        float intensity = (pixel.r + pixel.g + pixel.b) / 3f;
        float adjustedIntensity = Mathf.Pow(intensity, parameter);
        return new Color(adjustedIntensity, adjustedIntensity, adjustedIntensity);
    }

    private Color PiecewiseLinearTransformation(Color pixel)
    {
        // Apply piecewise linear transformation to the pixel intensity
        // Adjust the intensity based on the parameter value
        float parameter = parameterSlider.value;
        float intensity = (pixel.r + pixel.g + pixel.b) / 3f;

        // Define the breakpoints for the piecewise linear transformation
        float breakpoint1 = 0.33f;
        float breakpoint2 = 0.66f;

        // Apply the transformation based on the intensity range
        if (intensity <= breakpoint1)
        {
            return new Color(intensity * parameter, intensity * parameter, intensity * parameter);
        }
        else if (intensity <= breakpoint2)
        {
            return new Color(intensity, intensity, intensity);
        }
        else
        {
            return new Color(intensity / parameter, intensity / parameter, intensity / parameter);
        }
    }

    public enum IntensityTransformation
    {
        Linear,
        Logarithmic,
        Gamma,
        PiecewiseLinear
    }
}

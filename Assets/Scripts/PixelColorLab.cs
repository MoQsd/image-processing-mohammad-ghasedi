using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PixelColorLab : MonoBehaviour
{
    public Image originalImageDisplay;
    public Image processedImageDisplay;
    public Dropdown depthDropdown;

    private Texture2D _originalImage;
    private Texture2D _processedImage;
    
    public loadingbar loadingbar;
    public GameObject loadingObject;

    private int _selectedDepth = 8;

    private void Start()
    {
        _originalImage = originalImageDisplay.sprite.texture;

        // Load the original image at the start
        //LoadOriginalImage();
    }

    public void OnColorButtonClicked()
    {
        // Enable the loading object
        loadingObject.SetActive(true);
        loadingbar.SetLoadValue(0f);

        // Set the processed image as the original image
        Invoke(nameof(ConvertDepth), 0.1f);
    }

    private void ConvertDepth()
    {
        _processedImage = ConvertToTargetDepth(_originalImage, _selectedDepth);
        UpdateProcessedImageDisplay();
    }

    public void OnMonochromeButtonClicked()
    {
        // Enable the loading object
        loadingObject.SetActive(true);
        loadingbar.SetLoadValue(0f);
        
        // Convert the image to monochrome and update the processed image
        Invoke(nameof(CovertMonochrome), 0.1f);
    }

    private void CovertMonochrome()
    {
        _processedImage = ConvertToMonochrome(_originalImage, _selectedDepth);
        UpdateProcessedImageDisplay();
    }

    public void OnBinaryButtonClicked()
    {
        // Enable the loading object
        loadingObject.SetActive(true);
        loadingbar.SetLoadValue(0f);
        
        // Convert the image to binary and update the processed image
        Invoke(nameof(ConvertBinary), 0.1f);
    }

    private void ConvertBinary()
    {
        _processedImage = ConvertToBinary(_originalImage);
        UpdateProcessedImageDisplay();
    }

    public void OnGrayscaleButtonClicked()
    {
        // Enable the loading object
        loadingObject.SetActive(true);
        loadingbar.SetLoadValue(0f);

        // Convert the image to grayscale and update the processed image
        Invoke(nameof(ConvertGrayscale), 0.1f);
    }

    private void ConvertGrayscale()
    {
        _processedImage = ConvertToGrayscale(_originalImage, _selectedDepth);
        UpdateProcessedImageDisplay();
    }

    public void OnDepthDropdownChanged()
    {
        // Convert the image to the selected color mode based on the dropdown value
        switch (depthDropdown.value)
        {
            case 0:
                _selectedDepth = 8;
                break;
            case 1:
                _selectedDepth = 7;
                break;
            case 2:
                _selectedDepth = 6;
                break;
            case 3:
                _selectedDepth = 5;
                break;
            case 4:
                _selectedDepth = 4;
                break;
            case 5:
                _selectedDepth = 3;
                break;
            case 6:
                _selectedDepth = 2;
                break;
            case 7:
                _selectedDepth = 1;
                break;
            default:
                _selectedDepth = 8;
                break;
        }
    }


    /*public void LoadOriginalImage()
    {
        string imagePath = "";

        // Check the current platform
        if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor)
        {
            // Load image from file on PC
            imagePath = UnityEditor.EditorUtility.OpenFilePanel("Select Image", "", "png,jpg");
        }
        else if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
        {
            // Show a button to trigger the file picker dialog on mobile
            GameObject filePickerButton = new GameObject("FilePickerButton");
            filePickerButton.transform.SetParent(this.transform, false);

            Button button = filePickerButton.AddComponent<Button>();
            button.onClick.AddListener(() => OpenFilePicker());

            Text buttonText = filePickerButton.AddComponent<Text>();
            buttonText.text = "Select Image";
            buttonText.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
            buttonText.fontSize = 20;

            // Hide the original image display until an image is selected
            originalImageDisplay.gameObject.SetActive(false);
        }
        else
        {
            Debug.LogError("Unsupported platform! Cannot load image.");
            return;
        }

        // Load the image from the specified path (for PC platforms)
        if (!string.IsNullOrEmpty(imagePath))
        {
            StartCoroutine(LoadImageCoroutine(imagePath));
        }
    }*/

    /*private void OpenFilePicker()
    {
        // Trigger the file picker dialog
        StartCoroutine(PickImageCoroutine());
    }*/

    /*private IEnumerator PickImageCoroutine()
    {
        // Request permission to access the device's gallery
        yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);

        if (Application.HasUserAuthorization(UserAuthorization.WebCam))
        {
            // Show the file picker dialog
            var path = UnityEditor.EditorUtility.OpenFilePanel("Select Image", "", "png,jpg,jpeg");

            if (!string.IsNullOrEmpty(path))
            {
                // Load the image from the selected path
                StartCoroutine(LoadImageCoroutine(path));
            }
        }
        else
        {
            Debug.LogError("Permission denied! Cannot load image.");
        }
    }*/

    /*private IEnumerator LoadImageCoroutine(string imagePath)
    {
        // Create a WWW object to load the image
        WWW www = new WWW("file://" + imagePath);

        // Show the loading object
        loadingObject.SetActive(true);

        // Wait until the image is loaded
        yield return www;

        // Hide the loading object
        loadingObject.SetActive(false);

        if (string.IsNullOrEmpty(www.error))
        {
            // Assign the loaded image to the originalImage variable
            _originalImage = www.texture;

            // Set the original image display sprite as the loaded image
            originalImageDisplay.sprite = Sprite.Create(_originalImage, new Rect(0, 0, _originalImage.width, _originalImage.height), Vector2.zero);

            // Show the original image display
            originalImageDisplay.gameObject.SetActive(true);
        }
        else
        {
            Debug.LogError("Failed to load image: " + www.error);
        }
    }*/

    private void UpdateProcessedImageDisplay()
    {
        // Update the sprite of the processed image display with the processedImage texture
        processedImageDisplay.sprite = Sprite.Create(_processedImage, new Rect(0, 0, _processedImage.width, _processedImage.height), Vector2.zero);
    }

    private Texture2D ConvertToMonochrome(Texture2D sourceImage, int depth)
    {
        Texture2D monochromeImage = new Texture2D(sourceImage.width, sourceImage.height);

        int colorLevels = (int)Mathf.Pow(2, depth); // Calculate the number of color levels

        for (int x = 0; x < sourceImage.width; x++)
        {
            for (int y = 0; y < sourceImage.height; y++)
            {
                Color pixelColor = sourceImage.GetPixel(x, y);

                float intensity = (pixelColor.r + pixelColor.g + pixelColor.b) / 3;

                // Convert intensity to monochrome value based on the depth
                byte monochromeValue;
                if (depth == 1)
                {
                    // For depth 1, set monochrome value to 0 (black) if intensity < 0.5, otherwise 1 (green)
                    monochromeValue = intensity < 0.5f ? (byte)0 : (byte)colorLevels;
                }
                else
                {
                    // For other depths, calculate the monochrome value based on the intensity
                    monochromeValue = (byte)(intensity * (colorLevels));
                }

                // Scale the monochrome value back to the range [0, 1]
                float scaledValue = monochromeValue / (float)(colorLevels);

                // Set the pixel color of the monochrome image with green or black tone based on the depth
                Color pixelColorResult;
                if (depth == 1 && intensity < 0.5f)
                {
                    pixelColorResult = new Color(0, 0, 0); // Set black color
                }
                else
                {
                    pixelColorResult = new Color(0, scaledValue, 0); // Set green color
                }

                monochromeImage.SetPixel(x, y, pixelColorResult);
                
                float loadValue = (float)(y + 1) / sourceImage.height;
                loadingbar.SetLoadValue(loadValue);
            }
        }

        monochromeImage.Apply();

        Debug.Log("Converted to Monochrome! And depth = " + depth);
        loadingObject.SetActive(false);
        return monochromeImage;
    }


    private Texture2D ConvertToBinary(Texture2D sourceImage)
    {
        Texture2D binaryImage = new Texture2D(sourceImage.width, sourceImage.height);

        float threshold = 0.5f;

        for (int x = 0; x < sourceImage.width; x++)
        {
            for (int y = 0; y < sourceImage.height; y++)
            {
                Color pixelColor = sourceImage.GetPixel(x, y);

                // Calculate the average intensity value
                float intensity = (pixelColor.r + pixelColor.g + pixelColor.b) / 3;

                // Set the pixel color of the binary image based on the threshold
                byte binaryValue = intensity > threshold ? (byte)1 : (byte)0;

                // Set the pixel color to pure black or white
                Color binaryColor = binaryValue == 0 ? Color.black : Color.white;

                binaryImage.SetPixel(x, y, binaryColor);
                
                float loadValue = (float)(y + 1) / sourceImage.height;
                loadingbar.SetLoadValue(loadValue);
            }
        }

        binaryImage.Apply();

        loadingObject.SetActive(false);
        return binaryImage;
    }

    private Texture2D ConvertToGrayscale(Texture2D sourceImage, int depth)
    {
        Texture2D grayscaleImage = new Texture2D(sourceImage.width, sourceImage.height);

        int colorLevels = (int)Mathf.Pow(2, depth); // Calculate the number of color levels

        for (int x = 0; x < sourceImage.width; x++)
        {
            for (int y = 0; y < sourceImage.height; y++)
            {
                Color pixelColor = sourceImage.GetPixel(x, y);

                float intensity = (pixelColor.r + pixelColor.g + pixelColor.b) / 3;

                // Convert intensity to grayscale value based on the depth
                byte grayscaleValue;
                if (depth == 1)
                {
                    // For depth 1, set grayscale value to 0 (black) if intensity < 0.5, otherwise 1 (white)
                    grayscaleValue = intensity < 0.5f ? (byte)0 : (byte)colorLevels;
                }
                else
                {
                    // For other depths, calculate the grayscale value based on the intensity
                    grayscaleValue = (byte)(intensity * (colorLevels));
                }

                // Scale the grayscale value back to the range [0, 1]
                float scaledValue = grayscaleValue / (float)(colorLevels);

                // Set the pixel color of the grayscale image with scaled intensity value
                grayscaleImage.SetPixel(x, y, new Color(scaledValue, scaledValue, scaledValue));
                
                float loadValue = (float)(y + 1) / sourceImage.height;
                loadingbar.SetLoadValue(loadValue);
            }
        }

        grayscaleImage.Apply();

        Debug.Log("Converted to Grayscale! And depth = " + depth);
        loadingObject.SetActive(false);
        return grayscaleImage;
    }
    
    private Texture2D ConvertToTargetDepth(Texture2D sourceImage, int targetDepth)
    {
        Texture2D targetImage = new Texture2D(sourceImage.width, sourceImage.height);

        int sourceColorLevels = (int)Mathf.Pow(2, 8); // Assuming the source image has a depth of 8
        int targetColorLevels = (int)Mathf.Pow(2, targetDepth); // Calculate the number of color levels for the target depth

        for (int x = 0; x < sourceImage.width; x++)
        {
            for (int y = 0; y < sourceImage.height; y++)
            {
                Color pixelColor = sourceImage.GetPixel(x, y);

                // Calculate the color indexes based on the target depth
                byte redIndex = (byte)(pixelColor.r * (targetColorLevels - 1));
                byte greenIndex = (byte)(pixelColor.g * (targetColorLevels - 1));
                byte blueIndex = (byte)(pixelColor.b * (targetColorLevels - 1));

                // Scale the color indexes back to the range [0, 1]
                float redValue = redIndex / (float)(targetColorLevels - 1);
                float greenValue = greenIndex / (float)(targetColorLevels - 1);
                float blueValue = blueIndex / (float)(targetColorLevels - 1);

                // Set the pixel color of the target image with the guessed color
                Color targetColor = new Color(redValue, greenValue, blueValue);
                targetImage.SetPixel(x, y, targetColor);

                float loadValue = (float)(y + 1) / sourceImage.height;
                loadingbar.SetLoadValue(loadValue);
            }
        }

        targetImage.Apply();

        Debug.Log("Converted to Target Depth! Target depth = " + targetDepth);
        loadingObject.SetActive(false);
        return targetImage;
    }

}
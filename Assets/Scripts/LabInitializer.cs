using UnityEngine;
using UnityEngine.UI;

public class LabInitializer : MonoBehaviour
{
    public GameObject [] gameObjects;
    public Text labTitleText;
    
    private void Start()
    { 
        // Get the object to enable name from PlayerPrefs or a static variable
        var objectToEnableName = PlayerPrefs.GetString("ObjectToEnableName");
        var objectToEnable = new GameObject();

        if (!string.IsNullOrEmpty(objectToEnableName))
        {
            // Find the object to enable in the target scene by name

            foreach (var toEnable in gameObjects)
            {
                if (toEnable.name == objectToEnableName)
                { 
                    objectToEnable = toEnable;
                }
            }

            if (objectToEnable != null)
            {
                // Enable the specified object
                labTitleText.text = objectToEnableName;
                objectToEnable.SetActive(true);
            }
            else
            {
                Debug.LogWarning("Object to enable not found in the target scene.");
            }
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResolutionLab : MonoBehaviour
{
    public Image originalImageDisplay;

    public List<Image> spatialResImages;
    public int[] targetResolutions;

    public List<Image> intensityResImages;
    public int[] targetDepths;

    public GameObject spatialImagesGroup;
    public GameObject intensityImagesGroup;

    public loadingbar loadingbar;
    public GameObject loadingObject;

    public Text labTitleText;

    private Texture2D _originalImage;

    public CurrentResolutionType currentResolutionState;

    public enum CurrentResolutionType
    {
        SpatialResolution,
        IntensityResolution
    }


    private void Start()
    {
        currentResolutionState = CurrentResolutionType.SpatialResolution;
        _originalImage = originalImageDisplay.sprite.texture;

        // Load the original image at the start
        //LoadOriginalImage();
    }

    public void OnSpatialResolutionToggle()
    {
        //Show the spatial resolution UI and hide the intensity resolution UI
        ShowSpatialResolutionUI();
        HideIntensityResolutionUI();
        labTitleText.text = "Spatial Resolution Change";
        currentResolutionState = CurrentResolutionType.SpatialResolution;
    }

    public void OnIntensityResolutionToggle()
    {
        // Show the intensity resolution UI and hide the spatial resolution UI
        ShowIntensityResolutionUI();
        HideSpatialResolutionUI();
        labTitleText.text = "Intensity Resolution Change";
        currentResolutionState = CurrentResolutionType.IntensityResolution;
    }

    private void ShowSpatialResolutionUI()
    {
        spatialImagesGroup.SetActive(true);
    }

    private void HideSpatialResolutionUI()
    {
        spatialImagesGroup.SetActive(false);
    }

    private void ShowIntensityResolutionUI()
    {
        intensityImagesGroup.SetActive(true);
    }

    private void HideIntensityResolutionUI()
    {
        intensityImagesGroup.SetActive(false);
    }

    /*public void LoadOriginalImage()
    {
        string imagePath = "";

        // Check the current platform
        if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor)
        {
            // Load image from file on PC
            imagePath = UnityEditor.EditorUtility.OpenFilePanel("Select Image", "", "png,jpg");
        }
        else if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
        {
            // Show a button to trigger the file picker dialog on mobile
            GameObject filePickerButton = new GameObject("FilePickerButton");
            filePickerButton.transform.SetParent(this.transform, false);

            Button button = filePickerButton.AddComponent<Button>();
            button.onClick.AddListener(() => OpenFilePicker());

            Text buttonText = filePickerButton.AddComponent<Text>();
            buttonText.text = "Select Image";
            buttonText.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
            buttonText.fontSize = 20;

            // Hide the original image display until an image is selected
            originalImageDisplay.gameObject.SetActive(false);
        }
        else
        {
            Debug.LogError("Unsupported platform! Cannot load image.");
            return;
        }

        // Load the image from the specified path (for PC platforms)
        if (!string.IsNullOrEmpty(imagePath))
        {
            StartCoroutine(LoadImageCoroutine(imagePath));
        }
    }*/

    /*private void OpenFilePicker()
    {
        // Trigger the file picker dialog
        StartCoroutine(PickImageCoroutine());
    }

    private IEnumerator PickImageCoroutine()
    {
        // Request permission to access the device's gallery
        yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);

        if (Application.HasUserAuthorization(UserAuthorization.WebCam))
        {
            // Show the file picker dialog
            var path = UnityEditor.EditorUtility.OpenFilePanel("Select Image", "", "png,jpg,jpeg");

            if (!string.IsNullOrEmpty(path))
            {
                // Load the image from the selected path
                StartCoroutine(LoadImageCoroutine(path));
            }
        }
        else
        {
            //Debug.LogError("Permission denied! Cannot load image.");
        }
    }

    private IEnumerator LoadImageCoroutine(string imagePath)
    {
        // Create a WWW object to load the image
        WWW www = new WWW("file://" + imagePath);

        // Show the loading object
        loadingObject.SetActive(true);

        // Wait until the image is loaded
        yield return www;

        // Hide the loading object
        loadingObject.SetActive(false);

        if (string.IsNullOrEmpty(www.error))
        {
            // Assign the loaded image to the originalImage variable
            _originalImage = www.texture;

            // Set the original image display sprite as the loaded image
            originalImageDisplay.sprite = Sprite.Create(_originalImage, new Rect(0, 0, _originalImage.width, _originalImage.height), Vector2.zero);

            // Show the original image display
            originalImageDisplay.gameObject.SetActive(true);
        }
        else
        {
            //Debug.LogError("Failed to load image: " + www.error);
        }
    }*/


    /*private void ConvertToSpatialResolutions()
    {
        for (int i = 0; i < targetResolutions.Length; i++)
        {
            int resolution = targetResolutions[i]; // Extract the resolution value from the image name
            Texture2D resizedImage = ResizeImage(_originalImage, resolution, resolution);
            spatialResImages[i].sprite = Sprite.Create(resizedImage, new Rect(0, 0, resizedImage.width, resizedImage.height), Vector2.zero);
        }
    }

    private void ConvertToIntensityResolutions()
    {
        for (int i = 0; i < targetDepths.Length; i++)
        {
            int depth = targetDepths[i]; // Extract the resolution value from the image name
            Texture2D convertedImage = ConvertToTargetDepth(_originalImage, depth);
            intensityResImages[i].sprite = Sprite.Create(convertedImage, new Rect(0, 0, convertedImage.width, convertedImage.height), Vector2.zero);
        }
    }*/

    private Texture2D ResizeImage(Texture2D sourceImage, int targetWidth, int targetHeight)
    {
        Texture2D resizedImage = new Texture2D(targetWidth, targetHeight);
        RenderTexture rt = RenderTexture.GetTemporary(targetWidth, targetHeight);

        RenderTexture.active = rt;
        Graphics.Blit(sourceImage, rt);
        resizedImage.ReadPixels(new Rect(0, 0, targetWidth, targetHeight), 0, 0);
        resizedImage.Apply();

        RenderTexture.active = null;
        RenderTexture.ReleaseTemporary(rt);

        return resizedImage;
    }

    public void GenerateImages()
    {
        if (currentResolutionState == CurrentResolutionType.SpatialResolution)
        {
            StartCoroutine(ConvertToSpatialResolutionsCoroutine());
        }
        else
        {
            StartCoroutine(ConvertToIntensityResolutionsCoroutine());
        }
    }

    private IEnumerator ConvertToSpatialResolutionsCoroutine()
    {
        // Show the loading object
        loadingObject.SetActive(true);

        for (int i = 0; i < targetResolutions.Length; i++)
        {
            int resolution = targetResolutions[i]; // Extract the resolution value from the image name
            Texture2D resizedImage = ResizeImage(_originalImage, resolution, resolution);
            spatialResImages[i].sprite = Sprite.Create(resizedImage, new Rect(0, 0, resizedImage.width, resizedImage.height), Vector2.zero);

            // Update the load value of the loading bar
            float loadValue = (float)(i + 1) / targetResolutions.Length;
            loadingbar.SetLoadValue(loadValue);

            // Wait for a short delay before proceeding to the next conversion iteration
            yield return new WaitForSeconds(0.1f);
        }

        // Hide the loading object
        loadingObject.SetActive(false);
    }

    private IEnumerator ConvertToIntensityResolutionsCoroutine()
    {
        // Show the loading object
        loadingObject.SetActive(true);

        for (int i = 0; i < targetDepths.Length; i++)
        {
            int depth = targetDepths[i]; // Extract the resolution value from the image name
            Texture2D convertedImage = ConvertToTargetDepth(_originalImage, depth);
            intensityResImages[i].sprite = Sprite.Create(convertedImage, new Rect(0, 0, convertedImage.width, convertedImage.height), Vector2.zero);

            // Update the load value of the loading bar
            float loadValue = (float)(i + 1) / targetDepths.Length;
            loadingbar.SetLoadValue(loadValue);

            // Wait for a short delay before proceeding to the next conversion iteration
            yield return new WaitForSeconds(0.1f);
        }

        // Hide the loading object
        loadingObject.SetActive(false);
    }



    private Texture2D ConvertToTargetDepth(Texture2D sourceImage, int targetDepth)
    {
        Texture2D targetImage = new Texture2D(sourceImage.width, sourceImage.height);

        int sourceColorLevels = (int)Mathf.Pow(2, 8); // Assuming the source image has a depth of 8
        int targetColorLevels = (int)Mathf.Pow(2, targetDepth); // Calculate the number of color levels for the target depth

        for (int x = 0; x < sourceImage.width; x++)
        {
            for (int y = 0; y < sourceImage.height; y++)
            {
                Color pixelColor = sourceImage.GetPixel(x, y);

                // Calculate the color indexes based on the target depth
                byte redIndex = (byte)(pixelColor.r * (targetColorLevels - 1));
                byte greenIndex = (byte)(pixelColor.g * (targetColorLevels - 1));
                byte blueIndex = (byte)(pixelColor.b * (targetColorLevels - 1));

                // Scale the color indexes back to the range [0, 1]
                float redValue = redIndex / (float)(targetColorLevels - 1);
                float greenValue = greenIndex / (float)(targetColorLevels - 1);
                float blueValue = blueIndex / (float)(targetColorLevels - 1);

                // Set the pixel color of the target image with the guessed color
                Color targetColor = new Color(redValue, greenValue, blueValue);
                targetImage.SetPixel(x, y, targetColor);
            }
        }

        targetImage.Apply();

        //Debug.Log("Converted to Target Depth! Target depth = " + targetDepth);
        return targetImage;
    }
}

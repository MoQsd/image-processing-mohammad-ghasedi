using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadTargetScene : MonoBehaviour
{
    public void LoadMenuScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
}
